# Stylish GTK Theme (mod)

This is a fork of the [Stylish GTK Theme by vinceliuice](https://github.com/vinceliuice/stylish-gtk-theme) adding new colors as well as some minor fixes and optimizations for my personal use cases.

Colors added in this fork:

- "Mahe": `#88CC00`
- "Sakura": `#E76EAA`
- "Blue": `#51ADE0`

## Info

### GTK+ 3.20 or later

### GTK2 engines requirement

- GTK2 engine Murrine 0.98.1.1 or later.
- GTK2 pixbuf engine or the gtk(2)-engines package.

Fedora/RedHat distros:

    yum install gtk-murrine-engine gtk2-engines

Ubuntu/Mint/Debian distros:

    sudo apt-get install gtk2-engines-murrine gtk2-engines-pixbuf

ArchLinux:

    pacman -S gtk-engine-murrine gtk-engines

Other:
Search for the engines in your distributions repository or install the engines from source.

## Installation

Run

    ./Install

Usage:  `./Install`  **[OPTIONS...]**

|  OPTIONS:      | |
|:---------------|:-------------|
| -d, --dest     | Specify theme destination directory (Default: $HOME/.themes) |
| -n, --name     | Specify theme name (Default: stylish) |
| -v, --variant  | Specify theme light variant(s) **[standard/light/dark]** (Default: All variants) |
| -t, --color    | Specify hue theme variant(s) **[standard/tang/azul/jade/mahe/sakura/blue]** (Default: All variants) |
| -s, --size     | Specify theme size variant(s) **[standard/laptop]** (Default: All variants) |
| -h, --help     | Show this help |


## (Re)Build a specific color

```bash
export COLOR="Blue"

rm -f src/gtk-3.0/assets-$COLOR/assets/*.png
pushd src/gtk-3.0/assets-$COLOR; ./render-assets.sh; popd
rm -f src/gtk-2.0/assets/Stylish-$COLOR/*/*.png
pushd src/gtk-2.0/assets; ./render-assets.sh; popd
./parse-sass.sh
```